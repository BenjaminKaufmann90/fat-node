import io from 'socket.io-client';
import $ from 'jquery';

const socket = io('//localhost:3000');

$('form').submit(function(){
  if($('#chat-input').val().length) {
    socket.emit('chat message', $('#chat-input').val());

    $('#messages')
      .append($('<li class="me"><div class="content">' + $('#chat-input').val() + '</div></li>'));

    $('#chat-input').val('');
  }
  return false;
});

socket.on('chat message', function(msg){
  $('#messages').append($('<li class="bot"><div class="content">' + msg + '</div></li>'));
});

$('#chat-input').focus();
