/**
 *  simple dummy database for holding relevant data
 */
class DatabaseHandler {
  /**
   *
   */
  constructor() {
    this.data = {
      user: {
        firstName: 'Alice',
        lang: 'en',
        roomsBooked: [],
      },
      room: {
        available: {
          Room101: true,
          Room102: true,
          Room103: true,
          Room201: true,
          Room202: true,
          Room203: true,
        },
      },
    };
  }

  /**
   *
   * @returns {DatabaseHandler.data.user|{firstName, roomsBooked, lang}}
   */
  getUserData() {
    return this.data.user;
  }

  /**
   *
   * @returns {DatabaseHandler.data.room|{available}}
   */
  getRoomData() {
    return this.data.room;
  }

  /**
   *
   * @param data
   */
  setUserData(data) {
    this.data.user = Object.assign(this.data.user, data);
  }

  /**
   *
   * @param data
   */
  setRoomData(data) {
    this.data.room = Object.assign(this.data.room, data);
  }
}

exports.DatabaseHandler = DatabaseHandler;
