/**
  * The SocketHandler class handles sockets
  */
class SocketHandler {

  /**
    * Creates a new SocketHandler
    */
  constructor(messageHandler) {
    this.messageHandler = messageHandler;
  }

  /**
    * Logs that a user connected
    */
  onConnect(/* istanbul ignore next */{socket, c = console} = {}){
    c.log('a user connected');
    socket.emit('chat message', this.messageHandler.getConnectMessage());
  }

  /**
    * Logs that a user disconnected
    */
  onDisconnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user disconnected');
  }

  /**
    * Responds to a message
    */
  onMessage(/* istanbul ignore next */{msg, socket, c = console} = {}){
    c.log('message: ' + msg);
    // let reply = this.messageHandler.handleUserMessage(msg.replace(/\s/gi,'').toLowerCase());
    let reply = this.messageHandler.handleUserMessage(msg.trim());
    socket.emit('chat message', reply);
  }

}

exports.SocketHandler = SocketHandler;
