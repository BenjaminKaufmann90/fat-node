/**
 *  Class for handling user messages
 */
class MessageHandler {
  /**
   *
   * @param userData
   * @param translations
   * @param databaseHandler
   */
  constructor(userHandler, translations, databaseHandler) {
    this.userHandler = userHandler;
    this.databaseHandler = databaseHandler;
    this.userData = databaseHandler.getUserData();
    this.userLangTranslations = translations[this.userData.lang];
    this.lastMessage = '';
    this.next = null;
    this.current = null;
    this.reply = '';

    this.handleCancel = this.handleCancel.bind(this);
    this.handleHelp = this.handleHelp.bind(this);
    this.handleSights = this.handleSights.bind(this);
    this.handleRoomList = this.handleRoomList.bind(this);
    this.handleUserBookedRoomList = this.handleUserBookedRoomList.bind(this);
    this.handleBookingStep1 = this.handleBookingStep1.bind(this);
    this.handleBookingStep2 = this.handleBookingStep2.bind(this);
    this.handleBookingCancellationStep1 = this.handleBookingCancellationStep1.bind(this);
    this.handleBookingCancellationStep2 = this.handleBookingCancellationStep2.bind(this);



    this.responseTree = [
      {
        trigger: [
          this.userLangTranslations.command_cancel_1,
          this.userLangTranslations.command_cancel_2
        ],
        handler: this.handleCancel,
      },
      {
        trigger: [this.userLangTranslations.command_help],
        handler: this.handleHelp,
      },
      {
        trigger: [this.userLangTranslations.command_sights],
        handler: this.handleSights,
      },
      {
        trigger: [
          this.userLangTranslations.command_roomlist_1,
          this.userLangTranslations.command_roomlist_2
        ],
        handler: this.handleRoomList,
      },
      {
        trigger: [this.userLangTranslations.command_roomlist_booked],
        handler: this.handleUserBookedRoomList,
      },
      {
        trigger: [this.userLangTranslations.command_book],
        handler: this.handleBookingStep1,
        next: [
          {
            trigger: [
              this.userLangTranslations.command_cancel_1,
              this.userLangTranslations.command_cancel_2
            ],
            handler: this.handleCancel,
          },
          {
            trigger: [
              this.userLangTranslations.command_roomlist_1,
              this.userLangTranslations.command_roomlist_2
            ],
            handler: this.handleRoomList,
          },
          {
            trigger: ['USER_INPUT'],
            handler: this.handleBookingStep2,
          },
        ],
      },
      {
        trigger: [this.userLangTranslations.command_book_cancel],
        handler: this.handleBookingCancellationStep1,
        next: [
          {
            trigger: [
              this.userLangTranslations.command_cancel_1,
              this.userLangTranslations.command_cancel_2
            ],
            handler: this.handleCancel,
          },
          {
            trigger: [this.userLangTranslations.command_roomlist_booked],
            handler: this.handleUserBookedRoomList,
          },
          {
            trigger: ['USER_INPUT'],
            handler: this.handleBookingCancellationStep2,
          },
        ],
      }
    ];
  }

  /**
   *
   * @returns {string|string}
   */
  getReply() {
    return this.reply;
  }

  /**
   *
   * @param message
   */
  setReply(message) {
    this.reply = message;
  }

  /**
   * get text of message displayed on user connect
   */
  getConnectMessage() {
    return `
        ${this.userLangTranslations.chatWelcomePartOne} ${this.userData.firstName} 😘!
        ${this.userLangTranslations.chatWelcomePartTwo}
    `;
  }

  /**
   * try to cancel the current process and set the bot's response depending on the outcome
   */
  handleCancel() {
    var cancelableCommands = [
      this.userLangTranslations.command_book,
      this.userLangTranslations.command_book_cancel,
    ];
    if(cancelableCommands.indexOf(this.lastMessage.toLowerCase()) !== -1) {
      this.setReply(this.userLangTranslations.reply_cancel);
    } else {
      this.setReply(this.userLangTranslations.reply_cancel_error);
    }
  }

  /**
   * set the bot's response for a list of all possible commands
   */
  handleHelp() {
    this.reply = `${this.userLangTranslations.reply_help}
        <br><br>
        <strong>
            ${this.userLangTranslations.command_sights}
        </strong>          
          &rArr; ${this.userLangTranslations.reply_help_sights}
        <br>
        <strong>
            ${this.userLangTranslations.command_book}
        </strong>
         &rArr; ${this.userLangTranslations.reply_help_book}
        <br>
        <strong>
            ${this.userLangTranslations.command_book_cancel}
        </strong>
         &rArr;
          ${this.userLangTranslations.reply_help_book_cancel}
        <br>
        <strong>
          ${this.userLangTranslations.command_roomlist_1},
          ${this.userLangTranslations.command_roomlist_2}
        </strong>
         &rArr;
          ${this.userLangTranslations.reply_help_roomlist}
        <br>
        <strong>
            ${this.userLangTranslations.command_roomlist_booked}
        </strong>
         &rArr;
          ${this.userLangTranslations.reply_help_roomlist_booked}
        <br>
        <strong>
          ${this.userLangTranslations.command_cancel_1},
          ${this.userLangTranslations.command_cancel_2}
        </strong>
         &rArr;
          ${this.userLangTranslations.reply_help_cancel}
    `;
  }

  /**
   * set the bot's reply for the list of rooms currently available for booking
   */
  handleRoomList() {
    var availableRooms = this.databaseHandler.getRoomData().available;
    let reply = `${this.userLangTranslations.reply_roomlist}:<br>`;
    for(let roomName in availableRooms) {
      if(availableRooms[roomName] === true) {
        reply += `${roomName}<br>`;
      }
    }
    this.setReply(reply);
  }

  /**
   * set the bot's reply for the list of rooms the user has currently booked
   */
  handleUserBookedRoomList() {
    let reply;
    if(this.userData.roomsBooked.length > 0) {
      reply = `${this.userLangTranslations.reply_roomlist_booked}:<br>`;
      this.userData.roomsBooked.forEach(roomName => {
        reply += `${roomName}<br>`;
      });
    } else {
      reply = this.userLangTranslations.reply_roomlist_booked_empty;
    }
    this.setReply(reply);
  }

  /**
   * set the bot's reply for book command
   */
  handleBookingStep1() {
    this.setReply(this.userLangTranslations.reply_book);
  }

  /**
   * handle the booking of rooms and set the bot's reply depending on the booking's success
   * @param message
   * @returns {boolean}
   */
  handleBookingStep2(message) {
    let result = this.userHandler.bookRoom(message);
    let reply = result ?
      this.userLangTranslations.reply_book_success :
      this.userLangTranslations.reply_book_error;
    this.setReply(reply);
    return result;
  }

  /**
   * set the bot's reply for cancellation command
   */
  handleBookingCancellationStep1() {
    this.setReply(this.userLangTranslations.reply_book_cancel);
  }

  /**
   * handle the cancellation of booked rooms and set the bot's reply depending on the cancellation's success
   * @param message
   * @returns {boolean}
   */
  handleBookingCancellationStep2(message) {
    let result = this.userHandler.cancelRoom(message);
    let reply = result ?
      this.userLangTranslations.reply_book_cancel_success :
      this.userLangTranslations.reply_book_cancel_error;
    this.setReply(reply);
    return result;
  }

  /**
   * set the bot's reply for sights
   */
  handleSights() {
    this.setReply(`${this.userLangTranslations.reply_sights} 😎:<br>
        - ${this.userLangTranslations.reply_sights_1}<br>
        - ${this.userLangTranslations.reply_sights_2}<br>
        - ${this.userLangTranslations.reply_sights_3}
    `);
  }

  /**
   * sets the reply for messages that have no corresponding response object
   */
  handleUnknownMessage() {
    this.setReply(this.userLangTranslations.reply_unknownRequest);
  }

  /**
   * processes the response object that corresponds to the passed message and return the bot's reply as string
   * @param message
   * @returns {string|string}
   */
  handleUserMessage(message) {
    const responseTree = this.responseTree;

    let response;
    let messageCommand = message.toLowerCase();

    this.current = this.next || responseTree;

    response = this.current.find(
      response => (response.trigger.indexOf(messageCommand) !== -1 || response.trigger.indexOf('USER_INPUT') !== -1)
    );

    if(response) {
      this.next = response.next || null;
      response.handler(message);
      this.lastMessage = message;
    } else {
      this.handleUnknownMessage();
    }

    return this.getReply();
  }
}

exports.MessageHandler = MessageHandler;
