/**
 * class for handling bot actions and replies to user input
 */
class UserHandler {
  /**
     *
     * @param database
     */
  constructor(database) {
    this.database = database;
    this.userData = database.getUserData();
  }

  /**
     * if room <roomName> is available then set it as booked in the database
     * @param roomName
     * @returns {boolean}
     */
  bookRoom(roomName) {
    var roomsAvailable = this.database.getRoomData().available;
    if(
      roomName in roomsAvailable &&
      roomsAvailable[roomName] === true &&
      this.userData.roomsBooked.indexOf(roomName) === -1
    ) {
      //if (this.userData.roomsBooked.indexOf(roomName) === -1) {
      this.userData.roomsBooked.push(roomName);
      let updatedRoomAvailability = {};
      updatedRoomAvailability[roomName] = false;
      this.database.setRoomData({
        available: Object.assign(roomsAvailable, updatedRoomAvailability)
      });
      return true;
      //}
    }
    return false;
  }

  /**
     * cancel booking
     * @param roomName
     * @returns {boolean}
     */
  cancelRoom(roomName){
    var roomsAvailable = this.database.getRoomData().available;
    if(
      roomName in roomsAvailable &&
      roomsAvailable[roomName] === false &&
      this.userData.roomsBooked.indexOf(roomName) !== -1
    ) {
      var indexOfRoom = this.userData.roomsBooked.indexOf(roomName);
      let updatedRoomAvailability = {};
      this.userData.roomsBooked.splice(indexOfRoom, 1);
      console.log('this.userData.roomsBooked after splice', this.userData.roomsBooked);
      updatedRoomAvailability[roomName] = true;
      this.database.setRoomData({
        available: Object.assign(roomsAvailable, updatedRoomAvailability),
      });
      return true;
    }
    return false;
  }
}

exports.UserHandler = UserHandler;
