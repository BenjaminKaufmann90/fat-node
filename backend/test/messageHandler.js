var assert = require('assert');
var sinon = require('sinon');
var translations = require('./../src/translations.json');
var DatabaseHandler = require('./../src/databaseHandler.js');
var UserHandler = require('./../src/userHandler.js');
var MessageHandler = require('./../src/messageHandler.js');
const databaseHandler = new DatabaseHandler.DatabaseHandler();
const messageHandler = new MessageHandler.MessageHandler(
    new UserHandler.UserHandler(databaseHandler),
    translations,
    databaseHandler
);

describe('MessageHandler', function() {
    describe('#getReply', function() {
        it('it should return "test"', function() {
            messageHandler.setReply('test');
            assert.equal(messageHandler.getReply(), 'test', 'returned value is "test"');
        });
    });
    describe('#setReply', function() {
        it('it should set reply to "test2"', function() {
            messageHandler.setReply('test2');
            assert.equal(messageHandler.getReply(), 'test2', 'returned value is "test"');
        });
    });
    describe('#getConnectMessage', function() {
        it('it should return a string', function() {
            const connectMessage = messageHandler.getConnectMessage();
            assert.equal((typeof connectMessage === 'string'), true, 'returned value is a string');
        });
    });
    describe('#handleCancel', function() {
        it('it should return "' + translations.en.reply_cancel_error + '"', function() {
            messageHandler.handleCancel();
            assert.equal(messageHandler.getReply(), translations.en.reply_cancel_error);
        });
    });
    describe('#handleCancel', function() {
        it('it should return "' + translations.en.reply_cancel + '"', function() {
            messageHandler.handleUserMessage(translations.en.command_book);
            messageHandler.handleUserMessage(translations.en.command_cancel_1);
            assert.equal(messageHandler.getReply(), translations.en.reply_cancel);
        });
    });
    describe('#handleHelp', function() {
        it('it should return string', function() {
            messageHandler.handleHelp();
            assert.equal(typeof messageHandler.getReply() === 'string', true);
        });
    });
    describe('#handleRoomList', function() {
        it('it should return string', function() {
            messageHandler.handleRoomList();
            assert.equal(typeof messageHandler.getReply() === 'string', true);
        });
    });
    describe('#handleRoomList', function() {
        const reply = `${translations.en.reply_roomlist}:<br>Room101<br>`;
        it('it should return "' + reply + '"', function() {
            databaseHandler.setRoomData({
              available: {
                Room101: true, Room102: false, Room103: false, Room201: false, Room202: false,
              }
            });
            messageHandler.handleRoomList();
            assert.equal(messageHandler.getReply(), reply);
            databaseHandler.setRoomData({
                available: {
                    Room101: true, Room102: true, Room103: true, Room201: true, Room202: true,
                }
            });
        });
    });
    describe('#handleUserBookedRoomList', function() {
        it('it should return "' + translations.en.reply_roomlist_booked_empty + '"', function() {
            messageHandler.handleUserBookedRoomList();
            assert.equal(messageHandler.getReply(), translations.en.reply_roomlist_booked_empty);
        });
    });
    describe('#handleUserBookedRoomList', function() {
        const reply = `${translations.en.reply_roomlist_booked}:<br>Room101<br>`;
        it('it should return "' + reply + '"', function() {
            databaseHandler.setUserData({ roomsBooked: ['Room101'] });
            messageHandler.handleUserBookedRoomList();
            assert.equal(messageHandler.getReply(), reply);
            databaseHandler.setUserData({ roomsBooked: [] });
        });
    });
    describe('#handleBookingStep1', function() {
        it('it should return "' + translations.en.reply_book + '"', function() {
            messageHandler.handleBookingStep1();
            assert.equal(messageHandler.getReply(),  translations.en.reply_book);
        });
    });
    describe('#handleBookingStep2', function() {
        it('it should return true', function() {
            const message = 'Room101';
            const bookingResult = messageHandler.handleBookingStep2(message);
            assert.equal(bookingResult, true, 'room successfully booked');
        })
    });
    describe('#handleBookingCancellationStep1', function() {
        it('it should return "' + translations.en.reply_book_cancel + '"', function() {
            messageHandler.handleBookingCancellationStep1();
            assert.equal(messageHandler.getReply(),  translations.en.reply_book_cancel);
        });
    });
    describe('#handleBookingCancellationStep2', function() {
        it('it should return true', function() {
            const message = 'Room101';
            const cancellationResult = messageHandler.handleBookingCancellationStep2(message);
            assert.equal(cancellationResult, true, 'room successfully cancelled');
        })
    });
    describe('#handleSights', function() {
        it('it should return string', function() {
            messageHandler.handleSights();
            assert.equal(typeof messageHandler.getReply() === 'string', true);
        });
    });
    describe('#handleUnknownMessage', function() {
        it('it should set reply to "' + translations.en.reply_unknownRequest + '"', function() {
            messageHandler.handleUnknownMessage();
            assert.equal(messageHandler.getReply(), translations.en.reply_unknownRequest);
        })
    });
    describe('#handleUserMessage', function() {
        it('it should return "' + translations.en.reply_book + '"', function() {
            const message = translations.en.command_book;
            const result = messageHandler.handleUserMessage(message);
            assert.equal(result, translations.en.reply_book, 'the message "' + translations.en.reply_book + '" is being returned');
        })
    });
});
