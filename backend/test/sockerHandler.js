var assert = require('assert');
var sinon = require('sinon');
var SocketHandler = require('./../src/socketHandler');
var MessageHandler = require('./../src/messageHandler.js');
var translations = require('./../src/translations.json');
var DatabaseHandler = require('./../src/databaseHandler.js');
var UserHandler = require('./../src/userHandler.js');
const databaseHandler = new DatabaseHandler.DatabaseHandler();
const messageHandler = new MessageHandler.MessageHandler(
    new UserHandler.UserHandler(databaseHandler),
    translations,
    databaseHandler
);

describe('SocketHandler', function() {
  describe('#onConnect', function() {
    it('should log "a user connected"', function() {
      const input = {
        socket: {
          emit: sinon.spy()
        },
        c: {
          log: sinon.spy()
        }
      };
      const socketHandler = new SocketHandler.SocketHandler(messageHandler);
      socketHandler.onConnect(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'a user connected', 'Inappropriate message logged');
    });
  });

  describe('#onDisconnect', function() {
    it('should log "a user disconnected"', function() {
      const c = { log: sinon.spy() };
      const socketHandler = new SocketHandler.SocketHandler(messageHandler);
      socketHandler.onDisconnect({c})
      assert.equal(c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(c.log.getCall(0).args[0], 'a user disconnected', 'Inappropriate message logged');
    });
  });

  describe('#onMessage', function() {
    it('should respond with "' + translations.en.reply_unknownRequest + '" by default', function() {
      const input = {
        msg: '',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler(messageHandler);
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: ', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1], translations.en.reply_unknownRequest, 'Wrong reply emitted on socket');
    });
    it('should respond with "' + translations.en.reply_book + '"', function() {
      const input = {
        msg: translations.en.command_book,
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler(messageHandler);
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: ' + translations.en.command_book, 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1], translations.en.reply_book, 'Wrong reply emitted on socket');
    });
  });
});
