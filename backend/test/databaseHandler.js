var assert = require('assert');
var sinon = require('sinon');
var DatabaseHandler = require('./../src/databaseHandler.js');

describe('DatabaseHandler', function() {
    describe('#getUserData', function () {
        it('should return users complete data', function() {
            const databaseHandler = new DatabaseHandler.DatabaseHandler();
            const userData = databaseHandler.getUserData();
            assert.equal(userData.firstName, 'Alice', 'firstName should be "Alice"');
            assert.equal(userData.lang, 'en', 'language should be "en"');
        });
    });
    describe('#getRoomData', function () {
        it('should return complete room data', function() {
            const databaseHandler = new DatabaseHandler.DatabaseHandler();
            const roomData = databaseHandler.getRoomData();
            assert.equal("available" in roomData, true, "property 'available' must be in room data")
            for(roomName in roomData.available) {
                if(roomData.available.hasOwnProperty(roomName)) {
                    assert.equal(roomData.available[roomName], true, 'room "' + roomName + '" must be available indicated by setting its value to true');
                }
            }
        });
    });
});
