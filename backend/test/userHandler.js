var assert = require('assert');
var sinon = require('sinon');
var DatabaseHandler = require('./../src/databaseHandler.js');
var UserHandler = require('./../src/userHandler.js');
const databaseHandler = new DatabaseHandler.DatabaseHandler();
const userHandler = new UserHandler.UserHandler(databaseHandler);

describe('MessageHandler', function() {
    describe('#bookRoom', function () {
        it('should return false (booking nonexistent room)', function() {
            const roomName = 'Room12';
            const bookingResult = userHandler.bookRoom(roomName);
            assert.equal(bookingResult, false);
        });

        it('should return true (booking existent room)', function() {
            const roomName = 'Room101';
            const bookingResult = userHandler.bookRoom(roomName);
            assert.equal(bookingResult, true);
        });

        it('should return false (booking already booked room)', function() {
            const roomName = 'Room101';
            const bookingResult = userHandler.bookRoom(roomName);
            assert.equal(bookingResult, false);
        });
    });
    describe('#cancelRoom', function () {
        it('should return false (cancelling nonexisting room)', function() {
            const roomName = 'Room12';
            const cancellationResult = userHandler.cancelRoom(roomName);
            assert.equal(cancellationResult, false);
        });

        it('should return true (cancelling booked room)', function() {
            const roomName = 'Room101';
            const cancellationResult = userHandler.cancelRoom(roomName);
            assert.equal(cancellationResult, true);
        });

        it('should return false (cancelling unbooked room)', function() {
            const roomName = 'Room101';
            const cancellationResult = userHandler.cancelRoom(roomName);
            assert.equal(cancellationResult, false);
        });
    });
});
